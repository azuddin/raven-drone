import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.sass'],
})
export class HeaderComponent implements OnInit {
  currentUrl: string;
  isActiveMenu: boolean;
  constructor(private _router: Router) {
    _router.events.subscribe((_: NavigationEnd) => (this.currentUrl = _.url));
  }

  ngOnInit(): void {}
  public toggleMenu() {
    this.isActiveMenu = !this.isActiveMenu;
  }
}
