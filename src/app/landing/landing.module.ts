import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import {ReactiveFormsModule} from '@angular/forms';

import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { LandingComponent } from './landing.component';
import { HomeComponent } from './views/home/home.component';
import { AboutComponent } from './views/about/about.component';
import { PlatformComponent } from './views/platform/platform.component';
import { NewsComponent } from './views/news/news.component';
import { CareerComponent } from './views/career/career.component';
import { ProductsComponent } from './views/products/products.component';
import { ContactUsComponent } from './views/contact-us/contact-us.component';
import { PrivacyPolicyComponent } from './views/privacy-policy/privacy-policy.component';
import { TermsOfServiceComponent } from './views/terms-of-service/terms-of-service.component';
import { ResponsibleDisclosureComponent } from './views/responsible-disclosure/responsible-disclosure.component';
import { CustomerSupportComponent } from './views/customer-support/customer-support.component';

@NgModule({
  declarations: [
    HeaderComponent,
    FooterComponent,
    LandingComponent,
    HomeComponent,
    AboutComponent,
    PlatformComponent,
    NewsComponent,
    CareerComponent,
    ProductsComponent,
    ContactUsComponent,
    PrivacyPolicyComponent,
    TermsOfServiceComponent,
    ResponsibleDisclosureComponent,
    CustomerSupportComponent,
  ],
  imports: [RouterModule, CommonModule, ReactiveFormsModule],
  exports: [LandingComponent],
})
export class LandingModule {}
