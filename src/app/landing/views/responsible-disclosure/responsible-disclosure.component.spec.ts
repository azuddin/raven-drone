import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ResponsibleDisclosureComponent } from './responsible-disclosure.component';

describe('ResponsibleDisclosureComponent', () => {
  let component: ResponsibleDisclosureComponent;
  let fixture: ComponentFixture<ResponsibleDisclosureComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ResponsibleDisclosureComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ResponsibleDisclosureComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
