import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.sass'],
})
export class ProductsComponent implements OnInit {
  products = [
    {
      imgUrl: '/assets/product/backend.jpg',
      title: 'SaaS Platform',
      desc:
        "We create robust and intuitive platform for drone owner to automate their flight path. This then allow their drone's to perform multiple task or action automatically.",
    },
    {
      imgUrl: '/assets/product/allin1.jpg',
      title: 'All in one solution',
      desc:
        'All in one solution that help our customer to monitor and inspect their assets with percision and accuracy without hassles. All activities will be automated and AI assisted to return accurate and precise report regarding the assets.',
    },
    {
      imgUrl: '/assets/product/delivery.jpg',
      title: 'On demand delivery',
      desc:
        'Help logistic companies or restaurants to perform medium range delivery on demand. This will help increase efficiency during high peak seasons or hours.',
    },
  ];
  constructor() {}

  ngOnInit(): void {}
}
