import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-news',
  templateUrl: './news.component.html',
  styleUrls: ['./news.component.sass'],
})
export class NewsComponent implements OnInit {
  articles = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.getArticles();
  }

  private getArticles() {
    this.http
      .get(
        'http://newsapi.org/v2/everything?q=drone&nbsp;business&apiKey=ac8eabdab87b46f4b97944436230fd26'
      )
      .pipe(
        map((res) => {
          return res['articles'];
        })
      )
      .subscribe((res) => {
        this.articles = res;
      });
  }
}
