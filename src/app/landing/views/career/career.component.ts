import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-career',
  templateUrl: './career.component.html',
  styleUrls: ['./career.component.sass'],
})
export class CareerComponent implements OnInit {
  jobs = [
    {
      imgUrl: '/assets/career/designer.svg',
      title: 'UX/UI Designer',
      desc:
        'We are looking for a junior designer looking to gain hand-on experience craftingdigital products, apps, and websites. As a part of our design team you’ll be working on shaping productideas into stunning interface designs. You will have a strong understanding of startup founders’ vision,creating wireframe diagrams, branding proposals, prototypes and final UI designs. You will also collaborate tightly with the engineering team, explaining your designs and making changes when technical considerations arise. Want to build products used by millions? Look no further!',
    },
    {
      imgUrl: '/assets/career/developer.svg',
      title: 'Full-stack Developer',
      desc:
        'We are looking for a talented Full-StackDeveloper to join our team!',
    },
  ];

  constructor() {}

  ngOnInit(): void {}
}
