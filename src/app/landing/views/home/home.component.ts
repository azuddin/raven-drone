import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass'],
})
export class HomeComponent implements OnInit {
  articles = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.getArticles();
  }

  scrollTo(elem: HTMLElement) {
    elem.scrollIntoView({ behavior: 'smooth' });
  }

  private getArticles() {
    this.http
      .get(
        'http://newsapi.org/v2/everything?q=drone&nbsp;business&apiKey=ac8eabdab87b46f4b97944436230fd26&pageSize=3'
      )
      .pipe(
        map((res) => {
          return res['articles'];
        })
      )
      .subscribe((res) => {
        this.articles = res;
      });
  }
}
