import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.sass'],
})
export class CardComponent implements OnInit {
  isOnline: boolean;
  deviceId: number;

  constructor() {}

  ngOnInit(): void {}

  public toggleCard() {
    this.isOnline = !this.isOnline;
    console.log(this.isOnline);
  }
}
