import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { ClientComponent } from './client.component';
import { LoginComponent } from './views/login/login.component';
import { DashboardComponent } from './views/dashboard/dashboard.component';
import { EventsComponent } from './views/events/events.component';
import { ReportsComponent } from './views/reports/reports.component';
import { ShopComponent } from './views/shop/shop.component';
import { AccountComponent } from './views/account/account.component';
import { HeaderComponent } from './commons/header/header.component';
import { MenuComponent } from './commons/menu/menu.component';
import { CardComponent } from './commons/card/card.component';
import { CommonModule } from '@angular/common';
import { DevicesComponent } from './views/devices/devices.component';
import { DeviceComponent } from './views/devices/device/device.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [
    ClientComponent,
    LoginComponent,
    DashboardComponent,
    EventsComponent,
    ReportsComponent,
    ShopComponent,
    AccountComponent,
    HeaderComponent,
    MenuComponent,
    CardComponent,
    DevicesComponent,
    DeviceComponent,
  ],
  imports: [
    RouterModule,
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyDY5ycrsnNoQDVZbWsozfo_Q76jFgRPQzk',
    }),
  ],
  exports: [ClientComponent],
})
export class ClientModule {}
