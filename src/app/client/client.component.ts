import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-client',
  templateUrl: './client.component.html',
  styleUrls: ['./client.component.sass'],
})
export class ClientComponent implements OnInit {
  currentUrl: string;

  constructor(private _router: Router) {
    _router.events.subscribe((_: NavigationEnd) => (this.currentUrl = _.url));
  }

  ngOnInit(): void {}
}
