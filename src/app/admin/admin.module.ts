import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { LoginComponent } from './views/login/login.component';

@NgModule({
  declarations: [AdminComponent, LoginComponent],
  imports: [RouterModule],
  exports: [AdminComponent],
})
export class AdminModule {}
