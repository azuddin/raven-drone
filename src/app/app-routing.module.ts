import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LandingComponent } from './landing/landing.component';
import { HomeComponent } from './landing/views/home/home.component';
import { AboutComponent } from './landing/views/about/about.component';
import { PlatformComponent } from './landing/views/platform/platform.component';
import { ProductsComponent } from './landing/views/products/products.component';
import { NewsComponent } from './landing/views/news/news.component';
import { CareerComponent } from './landing/views/career/career.component';

import { ClientComponent } from './client/client.component';
import { LoginComponent as ClientLoginComponent } from './client/views/login/login.component';
import { DashboardComponent as ClientDashboardComponent } from './client/views/dashboard/dashboard.component';
import { EventsComponent as ClientEventsComponent } from './client/views/events/events.component';
import { ReportsComponent as ClientReportsComponent } from './client/views/reports/reports.component';
import { ShopComponent as ClientShopComponent } from './client/views/shop/shop.component';
import { AccountComponent as ClientAccountComponent } from './client/views/account/account.component';
import { DeviceComponent as ClientDeviceComponent } from './client/views/devices/device/device.component';

import { AdminComponent } from './admin/admin.component';
import { LoginComponent as AdminLoginComponent } from './admin/views/login/login.component';
import { ContactUsComponent } from './landing/views/contact-us/contact-us.component';
import { CustomerSupportComponent } from './landing/views/customer-support/customer-support.component';
import { PrivacyPolicyComponent } from './landing/views/privacy-policy/privacy-policy.component';
import { TermsOfServiceComponent } from './landing/views/terms-of-service/terms-of-service.component';
import { ResponsibleDisclosureComponent } from './landing/views/responsible-disclosure/responsible-disclosure.component';

const routes: Routes = [
  {
    path: '',
    component: LandingComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'about', component: AboutComponent },
      { path: 'platform', component: PlatformComponent },
      { path: 'product', component: ProductsComponent },
      { path: 'news', component: NewsComponent },
      { path: 'career', component: CareerComponent },
      { path: 'contact-us', component: ContactUsComponent },
      { path: 'customer-support', component: CustomerSupportComponent },
      { path: 'privacy-policy', component: PrivacyPolicyComponent },
      { path: 'terms-of-service', component: TermsOfServiceComponent },
      {
        path: 'responsible-disclosure',
        component: ResponsibleDisclosureComponent,
      },
    ],
  },
  {
    path: 'client',
    component: ClientComponent,
    children: [
      { path: 'login', component: ClientLoginComponent },
      {
        path: 'dashboard',
        children: [
          { path: '', component: ClientDashboardComponent },
          { path: 'device-details', component: ClientDeviceComponent },
        ],
      },
      { path: 'events', component: ClientEventsComponent },
      { path: 'reports', component: ClientReportsComponent },
      { path: 'shop', component: ClientShopComponent },
      { path: 'account', component: ClientAccountComponent },
    ],
  },
  {
    path: 'admin',
    component: AdminComponent,
    children: [{ path: 'login', component: AdminLoginComponent }],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
